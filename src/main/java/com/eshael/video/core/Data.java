package com.eshael.video.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Data
 */
public class Data extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String UPLOAD_DIRECTORY=System.getenv().get("OPENSHIFT_DATA_DIR")+"/uploads/";
	
	String name;
	
	private int BUFFER_LENGTH = 4096;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Data() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {  
    	
//       String filePath = request.getParameter("name");//substring(request.getContextPath().length());  
       String filePath = request.getPathInfo();
       
       System.out.println(filePath);
       //Decode url. Fixes issue with files having space within the file name  
       filePath = URLDecoder.decode(filePath, "UTF-8");  
       
       File file = new File(UPLOAD_DIRECTORY + filePath.replace("/", ""));  
       
       InputStream input = new FileInputStream(file);  
       response.setContentType("image/jpeg");
       response.setContentLength((int) file.length());
       OutputStream output = response.getOutputStream();  
       byte[] bytes = new byte[BUFFER_LENGTH];  
       int read = 0;  
       
       while ((read = input.read(bytes, 0, BUFFER_LENGTH)) != -1) 
       {  
            output.write(bytes, 0, read);  
            output.flush();  
       }  
       input.close();  
       output.close();  
  }  
    
    public byte[] getImageAsBytes (String ImageName) throws IOException {
        // open image
        File imgPath = new File(UPLOAD_DIRECTORY+ ImageName);
        BufferedImage bufferedImage = ImageIO.read(imgPath);

        // get DataBufferBytes from Raster
        WritableRaster raster = bufferedImage .getRaster();
        DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();

        return ( data.getData() );
   }
 
}
