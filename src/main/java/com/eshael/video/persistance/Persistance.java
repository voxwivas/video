package com.eshael.video.persistance;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.eshael.video.model.Category;
import com.eshael.video.model.Video;

public class Persistance {

	private static SessionFactory sessionFactory;
	
	public boolean CreateSession()
	{
		try 
		{
			sessionFactory = new Configuration().configure().buildSessionFactory();	
			if (sessionFactory != null)
				return true;
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean addVideo(Video video)
	{
		if(sessionFactory == null)
		{
			CreateSession();
		}
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();		
			session.save(video);
			transaction.commit();
			
			return true;
			
		} catch (HibernateException e) 
		{
			if(transaction != null) transaction.rollback();
			e.printStackTrace();
			
			return false;
			
		}finally
		{
			if(session != null)
				session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Video> listvideos()
	{
		if(sessionFactory == null)
		{
			CreateSession();
		}
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		List<Video> videos = new ArrayList<>();
		try 
		{
			videos = session.createQuery("FROM Video V ORDER BY V.id DESC").list();
			transaction = session.beginTransaction();
			transaction.commit();
			
			System.out.println("Matches : "+videos.size()+ " \n " + videos);
			return videos;
		} catch (HibernateException e)
		{
			if (transaction != null) transaction.rollback();
			e.printStackTrace();	
			
			return videos;
		}finally
		{
			if(session != null)
				session.close();
		}
	}
	public boolean addCategory(Category category)
	{
		if(sessionFactory == null)
		{
			CreateSession();
		}
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();		
			session.save(category);
			transaction.commit();
			
			return true;
			
		} catch (HibernateException e) 
		{
			if(transaction != null) transaction.rollback();
			e.printStackTrace();
			
			return false;
			
		}finally
		{
			if(session != null)
				session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Category> listCategories()
	{
		if(sessionFactory == null)
		{
			CreateSession();
		}
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		List<Category> categories = new ArrayList<>();
		try 
		{
			categories = session.createQuery("FROM Category C ORDER BY C.id DESC").list();
			transaction = session.beginTransaction();
			transaction.commit();
			
			System.out.println("Matches : "+categories.size()+ " \n " + categories);
			return categories;
		} catch (HibernateException e)
		{
			if (transaction != null) transaction.rollback();
			e.printStackTrace();	
			
			return categories;
		}finally
		{
			if(session != null)
				session.close();
		}
	}
	
}
