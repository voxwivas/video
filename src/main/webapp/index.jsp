<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Upload Video</h2>

	<form action="/video/upload" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td>Title :</td>
				<td><input type="text" name="title"/></td>
			</tr>
			<tr>
				<td>Duration : </td>
				<td><input type="text" name="duration"/></td>
			</tr>
			<tr>
				<td>Upload Thumbnail : </td>
				<td><input type="file" name="thumbnail"/></td>
			</tr>
			<tr>
				<td>Upload Video : </td>
				<td><input type="file" name="video"/></td>
			</tr>
			<tr>
				<td>Description  :</td>
				<td>
				<textarea rows="6" cols="25" name="description"></textarea>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Submit"/>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
